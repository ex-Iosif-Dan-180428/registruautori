package ro.ubb.remoting.common;

import java.io.Serializable;

/**
 * Created by alexa on 3/30/2018.
 */
public class BaseEntity<ID> implements Serializable{

    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }
}
