package ro.ubb.remoting.common;

public class Author extends Person {

    private String publisher;

    public Author(String publisher) {
        this.publisher = publisher;
    }

    public Author(String name, int age, String publisher) {
        super(name, age);
        this.publisher = publisher;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Author{" +
                "publisher='" + publisher + '\'' +
                '}';
    }
}
