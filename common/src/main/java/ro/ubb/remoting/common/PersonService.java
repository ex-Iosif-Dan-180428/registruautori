package ro.ubb.remoting.common;

import java.util.List;
import java.util.Map;

public interface PersonService<Person> {
    List<Person> findAll();

    void save(Person person);

    List<Person> sortedAuthorsByPublisherPersonsByAgeDesc();

    Map<String, Map<Integer, List<Author>>>authorsByPublisherAndAge();


}
