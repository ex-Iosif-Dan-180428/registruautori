package ro.ubb.remoting.client.UI;

import ro.ubb.remoting.client.service.PersonClientServiceImpl;
import ro.ubb.remoting.common.Author;
import ro.ubb.remoting.common.Person;

import java.util.List;
import java.util.Scanner;

public class ClientConsole {
    private PersonClientServiceImpl personClientService;

    public ClientConsole(PersonClientServiceImpl personClientService) {
        this.personClientService = personClientService;
    }

    private Scanner scanner = new Scanner(System.in);

    public void runConsole() {
        while (true) {
            startMenu();
            String option = scanner.next();
            if (option.equals("x")) break;
            switch (option) {
                case "1":
                    printAllPersons();
                    break;
                case "2":
                    addPersonOrAuthor();
                    break;
                default:
                    System.out.println("Invalid option!");
            }
        }
    }

    private void startMenu() {
        System.out.println("1. findAll");
        System.out.println("2. save ");
        System.out.println("3. Authors By Publisher Persons by Age");
        System.out.println("4. Authors By Publisher and Age");
    }

    private void printAllPersons() {
        List<Person> persons = personClientService.findAll();
        persons.forEach(System.out::println);
    }

    private void addPersonOrAuthor() {
        System.out.println("Enter name: ");
        String name = scanner.next();
        System.out.println("Enter age: ");
        int age  = scanner.nextInt();
        System.out.println("Enter publisher: ");
        String publisher = scanner.next();
        Author author = new Author(name, age, publisher);
        personClientService.save(author);
    }

}
