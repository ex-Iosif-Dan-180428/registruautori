package ro.ubb.remoting.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.stereotype.Component;
import ro.ubb.remoting.client.service.PersonClientServiceImpl;
import ro.ubb.remoting.common.PersonService;

@Component
public class PersonConfig {
    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean() {
        RmiProxyFactoryBean proxyFactoryBean = new RmiProxyFactoryBean();
        proxyFactoryBean.setServiceInterface(PersonService.class);
        proxyFactoryBean.setServiceUrl("rmi://localhost:1099/PersonService");
        return proxyFactoryBean;
    }

    @Bean
    PersonClientServiceImpl personService(){
        return new PersonClientServiceImpl();
    }
}
