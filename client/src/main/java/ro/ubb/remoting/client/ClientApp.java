package ro.ubb.remoting.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.client.UI.ClientConsole;
import ro.ubb.remoting.client.config.PersonConfig;
import ro.ubb.remoting.client.service.PersonClientServiceImpl;

public class ClientApp {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext personContext=
                new AnnotationConfigApplicationContext(PersonConfig.class);

        PersonClientServiceImpl personClientServiceImpl = personContext.getBean(PersonClientServiceImpl.class);

        ClientConsole clientConsole = new ClientConsole(personClientServiceImpl);
        clientConsole.runConsole();

    }
}
