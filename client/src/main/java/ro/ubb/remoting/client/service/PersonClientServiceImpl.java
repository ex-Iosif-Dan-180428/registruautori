package ro.ubb.remoting.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.remoting.common.Author;
import ro.ubb.remoting.common.Person;
import ro.ubb.remoting.common.PersonService;

import java.util.List;
import java.util.Map;

public class PersonClientServiceImpl implements PersonService<Person>{
    @Autowired
    private PersonService personService;

    @Override
    public List<Person> findAll() {
        return personService.findAll();
    }

    @Override
    public void save(Person person) {
        personService.save(person);
    }

    @Override
    public List<Person> sortedAuthorsByPublisherPersonsByAgeDesc() {
        return null;
    }

    @Override
    public Map<String, Map<Integer, List<Author>>> authorsByPublisherAndAge() {
        return null;
    }
}
