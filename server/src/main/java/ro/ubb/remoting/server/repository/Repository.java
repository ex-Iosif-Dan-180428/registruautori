package ro.ubb.remoting.server.repository;

import java.util.List;

/**
 * Created by alexa on 3/26/2018.
 */
public interface Repository<T> {
    List<T> findAll();
//    T findById(Long id);
    void save(T entity);
//    int update(T entity);
//    int deleteById(Long id);
}
