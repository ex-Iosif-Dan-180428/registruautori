package ro.ubb.remoting.server.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.Problem;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.remoting.common.Author;
import ro.ubb.remoting.common.Person;

import java.util.List;

public class PersonRepository implements Repository<Person> {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public List<Person> findAll() {
        String sql = "select * from persons";
        List<Person> persons = jdbcOperations.query(sql, (rs, i) -> {
            Long id = rs.getLong("id");
            String name = rs.getString("name");
            Integer age = rs.getInt("age");

            Person person = new Person(name, age);
            person.setId(id);
            return person;
        });

        return persons;
    }

    @Override
    public void save(Person person) {
        String sql = "insert into persons (name, age, publisher) values (?,?,?)";
        Author author = (Author)person;
        jdbcOperations.update(sql,  author.getName(), author.getAge(),author.getPublisher() );
    }
}
