package ro.ubb.remoting.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.common.Author;
import ro.ubb.remoting.common.Person;
import ro.ubb.remoting.common.PersonService;
import ro.ubb.remoting.server.config.JdbcConfig;
import ro.ubb.remoting.server.config.PersonConfig;

public class ServerApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext personContext=
                new AnnotationConfigApplicationContext(PersonConfig.class, JdbcConfig.class);

//        PersonService personService = personContext.getBean(PersonService.class);

//        Author author = new Author("ion", 22, "pub");
//        personService.save(author);
//
//        personService.findAll().forEach(System.out::println);


//
//        System.out.println(service.getById(3L));
//
//        Service problemService = problemContext.getBean(Service.class);
//        problemService.getAll().forEach(System.out::println);

        System.out.println("bye server");
    }
}
