package ro.ubb.remoting.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.remoting.common.Author;
import ro.ubb.remoting.common.BaseEntity;
import ro.ubb.remoting.common.PersonService;
import ro.ubb.remoting.server.repository.Repository;

import java.util.List;
import java.util.Map;

public class PersonServiceImpl<Person> implements PersonService<Person> {
    @Autowired
    private Repository<Person> repository;

    @Override
    public List findAll() {
        return repository.findAll();
    }

    @Override
    public void save(Person entity) {
        repository.save(entity);
    }

    @Override
    public List sortedAuthorsByPublisherPersonsByAgeDesc() {
        return null;
    }

    @Override
    public Map<String, Map<Integer, List<Author>>> authorsByPublisherAndAge() {
        return null;
    }
}
