package ro.ubb.remoting.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import ro.ubb.remoting.common.PersonService;
import ro.ubb.remoting.server.repository.PersonRepository;
import ro.ubb.remoting.server.service.PersonServiceImpl;

@Configuration
public class PersonConfig {
    @Bean
    RmiServiceExporter rmiServiceExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("PersonService");
        exporter.setServiceInterface(PersonService.class);
        exporter.setService(serverService());
        return exporter;
    }

    @Bean
    PersonRepository personRepository() {return new PersonRepository();}

    @Bean
    PersonService serverService() {return new PersonServiceImpl();  }
}
